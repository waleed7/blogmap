<?php

namespace Waleed\BlogMapBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Style\SymfonyStyle;


class ServeBlogCommand extends Command
{
    private $io;


    protected function configure()
    {
        $this
            ->setName('app:serve-blog')
            ->setDescription('Information about BlogMap!.')
            ->setHelp("display information about application and run it...");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
        $this->io->title('BlogMap!');

        while ($choice = $this->outputChoice()) {
            switch (strtolower($choice)):
                case 'general info':
                    $this->io->note('test task made for AqarMap using Symfony2');
                    break;
                case 'tools used':
                    $this->io->table(['Component', 'Version'], $this->getTools());
                    break;
                case 'apology':
                    $this->io->listing($this->getApologyList());
                    break;
                case 'version control':
                    $this->io->text("the Bitbucket repo can be found at https://goo.gl/hECuO8.");
                    $this->io->warning("excessive 'Branch per Feature' was used. which is mad given the project size, but i decided to do that for the purpose of demonstration ONLY.");
                    break;
                case 'exit':
                    exit;
                case 'serve the app':
                    $this->io->caution('Please make sure you ran "composer install" first');
                    $output->writeln('clearing cache');
                    $cache = $this->getApplication()->find('cache:clear');
                    $cache->run(new ArrayInput([]), $output);
                    $output->writeln('running server');
                    $server = $this->getApplication()->find('server:run');
                    $server->run(new ArrayInput([]), $output);

                    break;
                default:
                    // Symfony Command handle this
            endswitch;

        }

    }

    private function outputChoice()
    {
        $choice = $this->io->choice('Select Section',
            ['General Info', 'Tools Used', 'Version Control', 'Apology', 'Serve The App', 'Exit']);
        return $choice;
    }

    private function getTools()
    {
        return
            [
                ['Symfony', '2.7.1'],
                ['OS', 'Ubuntu Xenial 16.04'],
                ['PHP', 'PHP 7.0.8-0ubuntu0.16.04.3'],
                ['Web Server', 'PHP Built In Web Server'],
                ['Version Control', 'Git v2.7.4 locally and Bitbucket'],
                [
                    'Database',
                    'mysql  Ver 15.1 Distrib 10.0.27-MariaDB, for debian-linux-gnu (x86_64) using readline 5.2'
                ]
            ];

    }

    private function getApologyList()
    {
        return [
            "Symfony 2.7 usage : it was the last version i used, so it was the logical choice given the limited time. and it's a maintained version anyway",
            "Bad Design: i know it's seriously minimal and far from appealing, but what i understood from the email is that it was not an absolute prioirity",
            "Assetic: didn't have time to use it, and the size of design assets is so small that it would have been overkill",
            "Unit Testing: it's crucial but again the time limitation and the project scope made it extremely hard to dedicate time to it"
        ];
    }

}