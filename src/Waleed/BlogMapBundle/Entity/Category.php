<?php

namespace Waleed\BlogMapBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Category
 *
 * @ORM\Table(name="categories")
 * @ORM\Entity(repositoryClass="Waleed\BlogMapBundle\Entity\CategoryRepository")
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="category_name", type="string", length=255)
     * @Assert\NotBlank()
     * * @Assert\Length(
     *      min = 3,
     *      max = 60,
     *      minMessage = "Category Name must be at least 3 characters long",
     *      maxMessage = "Category Name must be at most 3 characters long"
     * )
     */
    private $categoryName;

    /**
     * @ORM\OneToMany(targetEntity="Post", mappedBy="category", fetch="EAGER",cascade={"persist", "remove"})
     */
    private $posts;

    public function __construct()
    {
        $this->posts = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categoryName
     *
     * @param string $categoryName
     * @return Category
     */
    public function setCategoryName($categoryName)
    {
        $this->categoryName = $categoryName;

        return $this;
    }

    /**
     * Get categoryName
     *
     * @return string 
     */
    public function getCategoryName()
    {
        return $this->categoryName;
    }

    /**
     * Add posts
     *
     * @param \Waleed\BlogMapBundle\Entity\Post $posts
     * @return Category
     */
    public function addPost(\Waleed\BlogMapBundle\Entity\Post $posts)
    {
        $this->posts[] = $posts;

        return $this;
    }

    /**
     * Remove posts
     *
     * @param \Waleed\BlogMapBundle\Entity\Post $posts
     */
    public function removePost(\Waleed\BlogMapBundle\Entity\Post $posts)
    {
        $this->posts->removeElement($posts);
    }

    /**
     * Get posts
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPosts()
    {
        return $this->posts;
    }
}
