<?php

namespace Waleed\BlogMapBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * PostRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PostRepository extends EntityRepository
{
    public function getActivePosts($category_id)
    {
        $qb = $this->createQueryBuilder('p')
            ->innerJoin('p.category', 'c')
            ->where('c.id = :categoryId')
            ->andWhere('p.active = :activePost')
            ->setParameter('categoryId', $category_id)
            ->setParameter('activePost', true);

        return $qb->getQuery()->getResult();
    }
}
