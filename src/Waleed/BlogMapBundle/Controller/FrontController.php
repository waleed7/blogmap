<?php

namespace Waleed\BlogMapBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Waleed\BlogMapBundle\Entity\Comment;
use Waleed\BlogMapBundle\Form\CommentType;


class FrontController extends Controller
{
    /*
     * Front page: listing of all posts
     */
    public function indexAction()
    {
        $post_repository = $this->getDoctrine()->getRepository('WaleedBlogMapBundle:Post');
        $posts = $post_repository->findByActive(1);
        return $this->render('WaleedBlogMapBundle:Front:index.html.twig', ['posts' => $posts]);
    }

    /*
     * Return single post
     * @param Slug string
     */
    public function viewAction($slug)
    {
        $post_repository = $this->getDoctrine()->getRepository('WaleedBlogMapBundle:Post');
        $post = $post_repository->findOneBy([
            'slug' => $slug,
            'active' => true
        ]);

        if (!$post) {
            throw $this->createNotFoundException(
                "This Post is Not Available"
            );
        }

        return $this->render('WaleedBlogMapBundle:Front:post.html.twig',
            [
                'post' => $post,
            ]);
    }

    /*
     * Return all categories for use in navigation
     */
    public function getAllCategoriesAction()
    {
        $category_repository = $this->getDoctrine()->getRepository('WaleedBlogMapBundle:Category');
        $categories = $category_repository->findAll();

        return $this->render('WaleedBlogMapBundle:Front:partials/nav/categories.html.twig',
            [
                'categories' => $categories,
            ]);
    }

    /*
     * Single Category page
     * 
     * @param Id category Id
     */
    public function categoryAction($id)
    {
        $category_repository = $this->getDoctrine()->getRepository('WaleedBlogMapBundle:Category');
        $category = $category_repository->find($id);

        if (!$category) {
            throw $this->createNotFoundException(
                "you are 100% category $id does exist? 100%?"
            );
        }

        $post_repository = $this->getDoctrine()->getRepository('WaleedBlogMapBundle:Post');
        $posts = $post_repository->getActivePosts($id);

        return $this->render('WaleedBlogMapBundle:Front:category.html.twig',
            [
                'category' => $category,
                'posts' => $posts,
            ]);
    }


    /**
     * Lists all Comment entities.
     *
     * @param String Post Id
     */
    public function commentsAction($postId)
    {
        $em = $this->getDoctrine()->getManager();

        $comments = $em->getRepository('WaleedBlogMapBundle:Comment')->findByPost([$postId]);

        return $this->render('WaleedBlogMapBundle:Front:partials/elements/comments.html.twig', [
            'comments' => $comments,
        ]);
    }

    /**
     * Displays a form to create a new Comment entity.
     *
     */
    public function newCommentAction($postId)
    {
        $entity = new Comment();
        $form = $this->createCommentForm($entity, $postId);

        return $this->render('WaleedBlogMapBundle:Front:partials/forms/comment.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a new Comment entity.
     *
     * @param Symfony\Component\HttpFoundation\Request
     * @param String Post Id
     */
    public function createCommentAction(Request $request, $postId)
    {
        $entity = new Comment();
        $form = $this->createCommentForm($entity, $postId);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();
            // TODO: refactor; error check
            $post = $this->getDoctrine()->getRepository('WaleedBlogMapBundle:Post')->find($postId);

            $this->addFlash(
                'notice',
                'Your Comment was added'
            );
            return $this->redirect($this->generateUrl('waleed_blog_map_post_view', ['slug' => $post->getSlug()]));
        }
        $this->addFlash(
            'notice',
            'Your Comment was not added'
        );
        return $this->render('WaleedBlogMapBundle:Front:partials/forms/comment.html.twig', array(
            'entity' => $entity,
            'form' => $form->createView(),
        ));
    }

    /**
     * Creates a form to create a Comment entity.
     *
     * @param Comment $entity The entity
     * @param String PostId
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCommentForm(Comment $entity, $postId)
    {
        $manager = $this->getDoctrine()->getManager();
        $form = $this->createForm(new CommentType($manager), $entity, [
            'action' => $this->generateUrl('comment_create', ['postId' => $postId]),
            'method' => 'POST',
            'postId' => $postId
        ]);

        $form->add('submit', 'submit', array('label' => 'Create', 'attr' => ['class' => 'btn btn-primary']));

        return $form;
    }


}
