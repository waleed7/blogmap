<?php

namespace Waleed\BlogMapBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class PostType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('postTitle','text')
            ->add('postContent','textarea')
            ->add('active','checkbox',['required' => false])
            ->add('category','entity',[
                'class' => 'Waleed\BlogMapBundle\Entity\Category',
                'choice_label' => 'category_name'
            ])
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Waleed\BlogMapBundle\Entity\Post'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'waleed_blogmapbundle_post';
    }
}
