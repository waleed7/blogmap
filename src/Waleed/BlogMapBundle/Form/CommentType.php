<?php

namespace Waleed\BlogMapBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\CallbackTransformer;
use Doctrine\Common\Persistence\ObjectManager;


class CommentType extends AbstractType
{

    private $manager;

    public function __construct(ObjectManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('content')
            ->add('post', 'hidden', [
                'data' => $options['postId'],
            ]);

        // just a simple data transformer will suffice here
        $builder->get('post')
            ->addModelTransformer(new CallbackTransformer(
                function ($post) {
                    return $post;
                },
                function ($postId) {
                    $post = $this->manager
                        ->getRepository('WaleedBlogMapBundle:Post')
                        ->find($postId);
                    return $post;
                }
            ));
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Waleed\BlogMapBundle\Entity\Comment'
        ));
        $resolver->setRequired(array(
            'postId'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'waleed_blogmapbundle_comment';
    }
}
